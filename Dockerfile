FROM openjdk:12

WORKDIR /reeke-api-bouchons

COPY Reeke-API-soapui-project.xml Reeke-API-soapui-project.xml

RUN curl https://s3.amazonaws.com/downloads.eviware/soapuios/5.5.0/SoapUI-5.5.0-linux-bin.tar.gz -o soapui.tar.gz
RUN tar zxvf soapui.tar.gz

EXPOSE 80

ENTRYPOINT [ "SoapUI-5.5.0/bin/mockservicerunner.sh", "-p", "80", "Reeke-API-soapui-project.xml" ]